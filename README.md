RPG Combat Kata
================

# Background #

This is a fun kata that has the programmer building simple combat rules, as for a role-playing game (RPG). It is implemented as a sequence of iterations. The domain doesn't include a map or any other character skills apart from their ability to damage and heal one another.

Complete each iteration before reading the next one. You may fix a time limit for each iteration as a constraint.

## Iteration One ##

1. All Characters, when created, have:
	- Health, starting at 1000
	- Level, starting at 1
	- May be Alive or Dead, starting Alive (Alive may be a true/false)

1. Characters can Deal Damage to Characters.
	- Damage is subtracted from Health
	- When damage received exceeds current Health, Health becomes 0 and the character dies

1. A Character can Heal a Character.
	- Dead characters cannot be healed
	- Healing cannot raise health above 1000

## Iteration Two ##

1. The Character can deal damage to his enemies, but not to himself.

1. The Character can heal himself, but not his enemies.

1. The level now has an effect on the damage applied:
	- If the target is 5 or more levels above the attacker, Damage is reduced by 50%
	- If the target is 5 or more levels below the attacker, Damage is increased by 50%

## Iteration Three ##

1. The Character has an attack range.

1. *Melee* fighters have a range of 2 meters.

1. *Ranged* fighters have a range of 20 meters.

1. When trying to deal damage, the Character must be in range.

## Retrospective ##

- Are you keeping up with the requirements? Has any iteration been a big challenge?
- Do you feel good about your design? Is it scalable and easily adapted to new requirements that will be introduced in the last iterations?
- Is everything tested and are you confident in your tests?

## Iteration Four ##

1. Characters may belong to one or more Factions.
	- Newly created Characters belong to no Faction.

1. A Character may Join or Leave one or more Factions.

1. Characters belonging to the same Faction are considered Allies.

1. Allies cannot Deal Damage to one another.

1. Allies can Heal one another.

## Iteration Five ##

Finally, the Character can damage other things that are not characters (props). This means that he can attack a house, a tree or anything else that has some health

1. Characters can damage non-character *things* (props).
	- Anything that has Health may be a target
	- These things cannot be Healed and they do not Deal Damage
	- These things do not belong to Factions; they are neutral
	- When reduced to 0 Health, things are *Destroyed*
	- As an example, you may create a Tree with 2000 Health

## Retrospective ##

- What problems did you encounter?
- What have you learned? Any new technique or pattern?
- Share your design with others, and get feedback on different approaches.
